import React from 'react';
import { nanoid } from 'nanoid';


const Meat = () => {
    return <div className="Meat"></div>
};
const MeatStack = props => {
    const text =[]
    for (let i = 0; i < props.num; i++) {
        text.push(<Meat key={nanoid()}/>);        
    }
    return text
};

export default MeatStack;