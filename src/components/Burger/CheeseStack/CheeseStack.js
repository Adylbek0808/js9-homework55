import React from 'react';
import { nanoid } from 'nanoid';


const Cheese = () => {
    return <div className="Cheese"></div>
};
const CheeseStack = props => {
    const text =[]
    for (let i = 0; i < props.num; i++) {
        text.push(<Cheese key={nanoid()}/>);        
    }
    return text
};

export default CheeseStack;