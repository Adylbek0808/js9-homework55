import React from 'react';
import './Burger.css';
import SaladStack from './SaladStack/SaladStack';
import CheeseStack from './CheeseStack/CheeseStack';
import MeatStack from './MeatStack/MeatStack';
import BaconStack from './BaconStack/BaconStack';


const Burger = props => {
    const comp = props.composition
    let salad, cheese, meat, bacon = {}
    for (let i = 0; i < comp.length; i++) {
        if (comp[i].name === "Salad") {
            salad = comp[i]
        };
        if (comp[i].name === "Cheese") {
            cheese = comp[i]
        };
        if (comp[i].name === "Meat") {
            meat = comp[i]
        };
        if (comp[i].name === "Bacon") {
            bacon = comp[i]
        }
    }
    return (
        <div className="Burger">
            <div className="BreadTop">
                <div className="Seeds1"></div>
                <div className="Seeds2"></div>
            </div>



            <SaladStack num={salad.count}></SaladStack>
            <CheeseStack num={cheese.count}></CheeseStack>
            <MeatStack num={meat.count}></MeatStack>
            <BaconStack num={bacon.count}></BaconStack>
            <div className="BreadBottom"></div>
        </div>
    );
};

export default Burger;