import React from 'react';
import { nanoid } from 'nanoid';


const Bacon = () => {
    return <div className="Bacon"></div>
};
const BaconStack = props => {
    const text =[]
    for (let i = 0; i < props.num; i++) {
        text.push(<Bacon key={nanoid()}/>);        
    }
    return text
};

export default BaconStack;