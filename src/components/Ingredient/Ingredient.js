import React from 'react';
import { Paper, Grid, Button, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        margin: '10px',
        padding: '10px'
    },
    image: {
        width: "50px",
        height: "50px"
    }
}));

const Ingredient = props => {
    const classes = useStyles();

    return (
        <Paper elevation={3} className={classes.root}>
            <Grid container alignItems="center">
                <Grid item xs={3}>
                    <img src={props.image} onClick={props.addItem} className={classes.image} alt="" />
                </Grid>
                <Grid item xs={4}>
                    <p>{props.name}</p>
                </Grid>
                <Grid item xs={3}>
                    <p>x{props.count}</p>
                </Grid>
                <Grid item xs={2}>
                    <Button variant="contained" onClick={props.removeItem}>Remove</Button>
                </Grid>
            </Grid>
        </Paper>
    );
};

export default Ingredient;