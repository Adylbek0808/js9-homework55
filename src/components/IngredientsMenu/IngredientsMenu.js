import React from 'react';
import Ingredient from '../Ingredient/Ingredient';

const IngredientsMenu = props => {
    return props.IngList.map(item => {
        return <Ingredient 
        key= {item.id} 
        name = {item.name} 
        count={item.count} 
        image = {item.image}
        addItem={() => props.addItem(item.id)}
        removeItem={() => props.removeItem(item.id)}
        />
    });
};
export default IngredientsMenu