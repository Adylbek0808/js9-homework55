import React, { useState } from 'react';
import './App.css';
import { Paper, makeStyles, Grid } from '@material-ui/core';
import Burger from '../../components/Burger/Burger';
import IngredientsMenu from '../../components/IngredientsMenu/IngredientsMenu';
import { nanoid } from 'nanoid';
import meatImage from './Images/meat.svg';
import cheeseImage from './Images/cheese.svg';
import saladImage from './Images/salad.svg';
import baconImage from './Images/bacon.svg';


const useStyles = makeStyles(theme => ({
  root: {
    margin: '10px',
    padding: '10px'
  }
}));

const INGREDIENTS = [
  { name: 'Meat', price: 50, image: meatImage },
  { name: 'Cheese', price: 20, image: cheeseImage },
  { name: 'Salad', price: 5, image: saladImage },
  { name: 'Bacon', price: 30, image: baconImage }
];
const baseCost = 20;

const App = () => {
  const classes = useStyles();

  const [ingredients, setIngredients] = useState(INGREDIENTS.map(item => {
    return (
      { id: nanoid(), name: item.name, count: 1, price: item.price, image: item.image }
    );
  }));



  const addItem = id => {
    const index = ingredients.findIndex(item => item.id === id);
    const itemCopy = ingredients[index];
    itemCopy.count++
    const ingredientsCopy = [...ingredients];
    ingredientsCopy[index] = itemCopy;
    setIngredients(ingredientsCopy);
  };

  const removeItem = id => {
    const index = ingredients.findIndex(item => item.id === id);
    const itemCopy = ingredients[index];
    if (itemCopy.count > 0) {
      itemCopy.count--
      const ingredientsCopy = [...ingredients];
      ingredientsCopy[index] = itemCopy;
      setIngredients(ingredientsCopy);
    };
  };

  const getTotalCost = () => {
    const meatTotal = ingredients.reduce((acc, item) => {
      if (item.name === "Meat") {
        return acc = item.count * item.price
      }
      return acc
    }, 0);
    const saladTotal = ingredients.reduce((acc, item) => {
      if (item.name === "Salad") {
        return acc = item.count * item.price
      }
      return acc
    }, 0);
    const cheeseTotal = ingredients.reduce((acc, item) => {
      if (item.name === "Cheese") {
        return acc = item.count * item.price
      }
      return acc
    }, 0);
    const baconTotal = ingredients.reduce((acc, item) => {
      if (item.name === "Bacon") {
        return acc = item.count * item.price
      }
      return acc
    }, 0);
    const total = baseCost + meatTotal + cheeseTotal + saladTotal + baconTotal
    return total
  };

  return (
    <Paper className={classes.root} elevation={1}>
      <h2 className="MainTitle">Hamburger Maker</h2>
      <div className="MainWindow">
        <Grid container spacing={1} >
          <Grid item xs={6}>
            <Paper className={classes.root} elevation={3}>
              <h3 className="MainTitle">Ingredients</h3>
              <IngredientsMenu IngList={ingredients} addItem={addItem} removeItem={removeItem}></IngredientsMenu>
            </Paper>
          </Grid>
          <Grid item xs={6} >
            <Paper className={classes.root} elevation={3} >
              <h3 className="MainTitle">Burger</h3>
              <Burger composition={ingredients} />
            </Paper>
            <p className="Cost">Total cost: {getTotalCost()} KGS</p>
          </Grid>
        </Grid>
      </div>
    </Paper>
  );
}

export default App;
